package cn.utility;




/**
 * <p>
 * 
 * </p>
 *
 * @author lnexin@aliyun.com
 * @since 2018-12-19
 */

public class City {



    public City() {
		super();
	}

	public City(Integer cid, String name, Integer pid) {
		super();
		this.cid = cid;
		this.name = name;
		this.pid = pid;
	}



	private Integer cid;
    private String name;
    private Integer pid;


    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    

    public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }



    @Override
    public String toString() {
        return "City{" +
        ", cid=" + cid +
        ", name=" + name +
        ", pid=" + pid +
        "}";
    }
}
