package cn.lnexin.security;

import org.junit.Test;

public class PasswordTest {
    
    
    @Test
    public void MD5Test() throws Exception {
        String encode = PasswordUtil.MD5Encode("admin", "UTF8", true);
        System.out.println();
        System.out.println("MD5算法:"+encode);
    }
    
    @Test
    public void aesTest() throws Exception {
        String content = "admin";
        String aesKey = "thisiskey";
        
        byte[] encrypt = PasswordUtil.AESEncrypt(content, aesKey);//加密
        System.out.println("AES加密后-byte[]:"+encrypt);
        
        String strHex = PasswordUtil.parseByte2HexStr(encrypt);
        System.out.println("转换后的十六进制:"+strHex);
        
        
        byte[] decrypt = PasswordUtil.AESDecrypt(PasswordUtil.parseHexStr2Byte(strHex), aesKey);//解密
        System.out.println("AES解密后-byte[]"+decrypt);
        System.out.println("AES解密转换后:"+new String(decrypt));
    }
    
}