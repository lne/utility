package cn.lnexin.format;

public class StringUtils {
	
	public static boolean isEmpty(String value) {
		return (value == null) || (value.trim().length() == 0);
	}
	
	public static int[] strToIntArray(String data, String split) {
		String[] fields = data.split(split);
		int i = 0;
		int[] ret = new int[fields.length];
		for (String field : fields) {
			ret[i++] = Integer.parseInt(field);
		}
		return ret;
	}
        public static void main(String[] args) {
        System.out.println(RNDText());
    }

    public static Random RND = new Random();

    public static String RNDText() {
        byte[] arr = new byte[16];
        RND.nextBytes(arr);
        String hexString = toHexStr(arr, "");
        return hexString;
    }


    public static String toHexStr(byte[] data, String span) {
        StringBuilder strBuilder = new StringBuilder();
        for (byte b : data) {
            strBuilder.append(Integer.toHexString((b & 0xFF)));
            if (span != null) {
                strBuilder.append(span);
            }
        }
        if (strBuilder.length() > 0) {
            strBuilder.deleteCharAt(strBuilder.length() - 1);
        }
        return strBuilder.toString();
    }
}
