package cn.lnexin.format;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularMatch {
	public static String match(String data, String regexp) {
		String result = "";
		Pattern p = Pattern.compile(regexp);
		Matcher m = p.matcher(data);
		while (m.find()) {
			result = m.group(0);
		}
		return result;
	}
}
