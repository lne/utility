package cn.lnexin.format;

public class DEVFormat {
	/**
	 * 32位
	 * 
	 * @param value
	 * @return
	 */
	public static String IMEI(String value) {
		String result = null;
		if (value == null || value.equals("")) {
			result = "00000000-0000-0000-0000-000000000000";
//		} else if (value.length() < 32) {
//			return result;
		} else if (value.length() == 32) {
			StringBuilder sb = new StringBuilder();
			sb.append(value.substring(0, 8)).append("-");
			sb.append(value.substring(8, 12)).append("-");
			sb.append(value.substring(12, 16)).append("-");
			sb.append(value.substring(16, 20)).append("-");
			sb.append(value.substring(20, 32));
			result = sb.toString();
		} else {
			value = value.replaceAll("\\.", "-");
			value = value.replaceAll(" ", "-");
			value = value.replaceAll("_", "-");
			value = value.replaceAll(":", "-");
			value = value.replaceAll("\"", "-");
			result = value;
		}
		result = result.toUpperCase();
		return result;
	}
	
	public static String Mac(String value) {
		String result = null;
		if (value == null || value.equals("")) {
			return result;
		} else if (value.length() == 12) {
			StringBuilder sb = new StringBuilder();
			sb.append(value.substring(0, 2)).append(":");
			sb.append(value.substring(2, 4)).append(":");
			sb.append(value.substring(4, 6)).append(":");
			sb.append(value.substring(6, 8)).append(":");
			sb.append(value.substring(8, 10)).append(":");
			sb.append(value.substring(10, 12));
			result = sb.toString();
		} else {
			value = value.replaceAll("-", ":");
			value = value.replaceAll("\\.", ":");
			value = value.replaceAll("_", ":");
			value = value.replaceAll(" ", ":");
			value = value.replaceAll("\"", "-");
			result = value;
		}
		result = result.toUpperCase();
		return result;
	}
	
	public static String IP(String value) {
		String result = null;
		if (value.startsWith("/")) {
			result = value.substring(1);
		}
		String reg = "((25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d)))\\.){3}(25[0-5]|2[0-4]\\d|((1\\d{2})|([1-9]?\\d)))";
		result = RegularMatch.match(value, reg);
		return result;
	}
	
}
