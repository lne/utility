package cn.lnexin.net;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import cn.lnexin.format.JsonUtil;
import cn.lnexin.format.RegularMatch;

public class SelectIPAddressUtil {
	private final static String SINA_API_URL = "http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js&ip=";
	private final static String TAOBAO_API_URL = "http://ip.taobao.com/service/getIpInfo.php?ip=";
	
	public static Map<String, String> search(long ip) {
		return search(ipInt2Str(ip));
	}
	
	public static Map<String, String> search(String ip) {
		Map<String, String> address = new LinkedHashMap(5);
		address.put("ipstr", ip);
		address.put("ipint", String.valueOf(ipStr2Int(ip)));
		
		JsonNode s_result = searchSina(ip);
		String country = find("country", s_result);
		String province = find("province", s_result);
		String city = find("city", s_result);
		if (province.isEmpty() || city.isEmpty()) {
			JsonNode tb_result = searchTaobao(ip);
			country = find("country", tb_result);
			province = find("region", tb_result);
			if (province.equals("XX")) province = country;
			city = find("city", tb_result);
			if (city.equals("XX")) city = province;
		}
		address.put("country", country);
		address.put("province", province);
		address.put("city", city);
		return address;
	}
	
	private static JsonNode searchSina(String ip) {
		try {
			String result = HttpClientUtil.sendGet(SINA_API_URL + ip);
			String data = RegularMatch.match(result, "\\{[\\S\\w]+\\}");
			return JsonUtil.getJsonNode(data);
		} catch (IOException e) {
			System.err.println("新浪接口IP查询出错!" + ip + e);
			return null;
		}
	}
	
	private static JsonNode searchTaobao(String ip) {
		try {
			String result = HttpJavaUtil.sendPostRequest(TAOBAO_API_URL + ip, "", "UTF8").get("respBody");
			return JsonUtil.getJsonNode(result);
		} catch (IOException e) {
			System.err.println("淘宝接口IP查询出错!" + ip + e);
			return null;
		}
	}
	
	private static String find(String key, JsonNode node) {
		try {
			JsonNode value = node.findValue(key);
			return value.asText();
		} catch (Exception e) {
		}
		return null;
	}
	
	public static long ipStr2Int(String strIp) {
		long[] ip = new long[4];
		int position1 = strIp.indexOf(".");
		int position2 = strIp.indexOf(".", position1 + 1);
		int position3 = strIp.indexOf(".", position2 + 1);
		ip[0] = Long.parseLong(strIp.substring(0, position1));
		ip[1] = Long.parseLong(strIp.substring(position1 + 1, position2));
		ip[2] = Long.parseLong(strIp.substring(position2 + 1, position3));
		ip[3] = Long.parseLong(strIp.substring(position3 + 1));
		return (ip[0] << 24) + (ip[1] << 16) + (ip[2] << 8) + ip[3];
	}
	
	public static String ipInt2Str(long ip) {
		StringBuffer sb = new StringBuffer();
		// 右移动24位,得出第一段
		sb.append(String.valueOf(ip >>> 24)).append(".");
		// 高8位补0,右移16位,得出第二段
		sb.append(String.valueOf((ip & 0x00FFFFFF) >>> 16)).append(".");
		sb.append(String.valueOf((ip & 0x0000FFFF) >>> 8)).append(".");
		sb.append(String.valueOf(ip & 0x000000FF));
		return sb.toString();
	}
	
}
