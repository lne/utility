package cn.utility;

import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author lnexin@aliyun.com
 * @since 2018-12-19
 */

public class Province  {



    private Integer id;
    private String name;

    private String enName;

    private String enShortName;

    private String shortName;
    /**
     * 区域
     */
    private String area;
    private Integer code;

    
    private City capital;
    private List<Province> adjoins;
    private List<City> citys;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getEnShortName() {
        return enShortName;
    }

    public void setEnShortName(String enShortName) {
        this.enShortName = enShortName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
    
    
    public City getCapital() {
		return capital;
	}

	public void setCapital(City capital) {
		this.capital = capital;
	}

	public List<Province> getAdjoins() {
		return adjoins;
	}

	public void setAdjoins(List<Province> adjoins) {
		this.adjoins = adjoins;
	}

	public List<City> getCitys() {
		return citys;
	}

	public void setCitys(List<City> citys) {
		this.citys = citys;
	}

	@Override
	public String toString() {
		return "Province [id=" + id + ", name=" + name + ", enName=" + enName + ", enShortName=" + enShortName + ", shortName=" + shortName + ", area=" + area + ", code=" + code + ", capital=" + capital + ", adjoins=" + adjoins + ", citys=" + citys + "]";
	}


}
