package cn.lnexin.format;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 * String common operation
 * 
 * @author Daniel
 * @method
 *         <li>boolean isEmpty(String value)
 *         <li>int[] strToIntArray(String data, String split)
 *         <li>分割字符串 (AtomicInteger 用于多线程下，可提供原子操作)
 *         <li>String NextSegment(String src, String splitstr, Integer point)
 *         <li>String NextSegment(String src, String beginstr, String endstr, Integer point)
 *         <li>String NextSegment(String src, String splitstr, AtomicInteger point)
 *         <li>String NextSegment(String src, String beginstr, String endstr, AtomicInteger point)
 */
public class StrUtil {
	public static void main(String[] args) {
		String old = "MyStrxxx=bababa\tname=heiheihei\tkey=youDIA电脑%age=2203";
		String[] split1 = old.split("\\t");
		System.out.println(Arrays.toString(split1));
		for (int i = 0; i < split1.length; i++) {
			String string = NextSegment(old, "\t ",0);
			System.out.println(i + ":" + string);
		}
	}
	
	public static boolean isEmpty(String value) {
		return (value == null) || (value.trim().length() == 0);
	}
	
	public static int[] strToIntArray(String data, String split) {
		String[] fields = data.split(split);
		int i = 0;
		int[] ret = new int[fields.length];
		for (String field : fields) {
			ret[i++] = Integer.parseInt(field);
		}
		return ret;
	}
	
	public static String NextSegment(String src, String splitstr, Integer point) {
		try {
			if (point >= src.length()) {
				return null;
			}
			int next = src.indexOf(splitstr, point);
			if (next == -1) {
				next = src.length();
			}
			String re = src.substring(point, next);
			point = next + splitstr.length();
			return re;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String NextSegment(String src, String beginstr, String endstr, Integer point) {
		try {
			if (point >= src.length()) {
				return null;
			}
			int start = src.indexOf(beginstr, point) + beginstr.length();
			int end = src.indexOf(endstr, start);
			if (start == -1) {
				return null;
			}
			if (end == -1) {
				end = src.length();
			}
			point = end + endstr.length();
			if (start == end) {
				return "";
			} else {
				return src.substring(start, end);
			}
		} catch (Exception e) {
			point = src.length();
			return null;
		}
	}
	
	public static String NextSegment(String src, String splitstr, AtomicInteger point) {
		try {
			if (point.get() >= src.length()) {
				return null;
			}
			int next = src.indexOf(splitstr, point.get());
			if (next == -1) {
				next = src.length();
			}
			String re = src.substring(point.get(), next);
			point.set(next + splitstr.length());
			return re;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static String NextSegment(String src, String beginstr, String endstr, AtomicInteger point) {
		try {
			if (point.get() >= src.length()) {
				return null;
			}
			int start = src.indexOf(beginstr, point.get()) + beginstr.length();
			int end = src.indexOf(endstr, start);
			if (start == -1) {
				return null;
			}
			if (end == -1) {
				end = src.length();
			}
			point.set(end + endstr.length());
			if (start == end) {
				return "";
			} else {
				return src.substring(start, end);
			}
		} catch (Exception e) {
			point.set(src.length());
			return null;
		}
	}
}
