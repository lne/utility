package cn.lnexin.format;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConvertUtil {
	
	/**
	 * 根据指定的格式得到时间戳
	 * 
	 * @param pattern
	 *            指定的格式
	 * @return 指定格式的时间戳
	 */
	public static String getDate(String pattern, Date date) {
		return new SimpleDateFormat(pattern).format(date);
	}
	
	public static Date long2date(String timestamp) {
		if (timestamp.length() < 13) {
			timestamp = (timestamp + "0000000000000").substring(0, 13);
		}
		return long2date(Long.valueOf(timestamp));
	}
	
	public static Date long2date(Long timestamp) {
		Timestamp date = new Timestamp(timestamp);
		return date;
	}
	
	/**
	 * 提取一个月中的最后一天
	 * 
	 * @param day
	 * @return
	 */
	public static Date getLastDayByMonth(long day) {
		Date date = new Date();
		long date_3_hm = date.getTime() - 3600000 * 34 * day;
		Date date_3_hm_date = new Date(date_3_hm);
		return date_3_hm_date;
	}
	
	public static Date str2Date(String pattern, String date) {
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		try {
			return sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static long date2Long(String pattern, String date) {
		return str2Date(pattern, date).getTime();
	}
	
	public static long date2Long(Date date) {
		return date.getTime();
	}
	
}
