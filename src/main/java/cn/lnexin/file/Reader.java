package cn.lnexin.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;

public class Reader {
	public static BufferedReader getBufferReader(File file) throws IOException {
		return getBufferReader(file.getPath());
	}
	
	public static BufferedReader getBufferReader(String filePath) throws IOException {
		if (filePath.endsWith(".gz")) {
			return getGZReader(filePath);
		} else {
			return getTxtReader(filePath);
		}
	}
	
	private synchronized static BufferedReader getTxtReader(String filePath) throws IOException {
		InputStreamReader in = new InputStreamReader(new FileInputStream(filePath), "UTF-8");
		BufferedReader reader = new BufferedReader(in);
		return reader;
	}
	
	private synchronized static BufferedReader getGZReader(String filePath) throws IOException {
		File file = new File(filePath);
		InputStream in = new GZIPInputStream(new FileInputStream(file));
		BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF8"));
		return reader;
	}
	
	public synchronized static BufferedReader getMGZReader(String filePath) throws IOException {
		File file = new File(filePath);
		MultiGZIPInputStream in = new MultiGZIPInputStream(new FileInputStream(file));
		BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF8"));
		return reader;
	}
	
	/*
	 * 在解压gz文件时，如果直接用java.util.zip.GZIPInputStream来处理问题只能解压很少一部分内容，通过类MultiMemberGZIPInputStream
	 * 可以完全解压一个gz文件。
	 */
	
}