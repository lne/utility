package cn.lnexin.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class FileUtil {
	
	public static List<String> getFileList(String src_dir) {
		getList(src_dir);
		return fileList;
	}
	
	public static List<String> getFileList(String src_dir, List<String> limit_names) {
		getListByLimit(src_dir);
		return fileList;
	}
	
	public static BufferedReader getGZReader(String filePath) {
		File file = new File(filePath);
		try {
			InputStream in = new GZIPInputStream(new FileInputStream(file));
			BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF8"));
			return reader;
		} catch (Exception e) {
			System.err.println("获取BufferedReader失败!" + filePath);
		}
		return null;
	}
	
	public static BufferedReader getTxtReader(String filePath) {
		try {
			InputStreamReader in = new InputStreamReader(new FileInputStream(filePath), "UTF-8");
			BufferedReader reader = new BufferedReader(in);
			return reader;
		} catch (Exception e) {
			System.err.println("获取BufferedReader失败!" + filePath);
		}
		return null;
	}
	
	static List<String> fileList = new ArrayList<>();
	
	private static void getList(String src_dir) {
		File file = new File(src_dir);
		if (!file.isFile() && file.isDirectory()) {
			File[] fileLists = file.listFiles();
			for (int i = 0; i < fileLists.length; i++) {
				File child = fileLists[i];
				if (child.isDirectory()) {
					getList(fileLists[i].getPath());
				}
				if (fileLists[i].isFile()) {
					fileList.add(fileLists[i].getPath());
				}
			}
		}
	}
	
	private static void getListByLimit(String src_dir) {
		File file = new File(src_dir);
		List<String> names = getNames();
		if (!file.isFile() && file.isDirectory()) {
			File[] fileLists = file.listFiles();
			for (int i = 0; i < fileLists.length; i++) {
				File child = fileLists[i];
				if (child.isDirectory() && names.contains(child.getName())) {
					getListByLimit(fileLists[i].getPath());
				}
				if (fileLists[i].isFile() && names.contains(child.getParentFile().getName())) {
					fileList.add(fileLists[i].getPath());
				}
			}
		}
	}
	
	private static List<String> getNames() {
		List<String> names = new ArrayList<>();
		for (int i = 1001; i < 9000; i++) {
			names.add(String.valueOf(i));
		}
		return names;
	}
	
	public static String[] filterDir(String filter_str, String src_dir) {
		ArrayList<String> list = new ArrayList<String>();
		File file = new File(src_dir);
		File[] files = file.listFiles();
		for (int i = 0; i < files.length; i++) {
			if (!files[i].isDirectory()) {
				if (files[i].getName().indexOf(filter_str) != -1) {
					list.add(files[i].getName());
				}
			}
		}
		if (list.size() > 0) {
			String[] re = new String[list.size()];
			for (int i = 0; i < list.size(); i += 1) {
				re[i] = list.get(i);
			}
			return re;
		} else {
			return null;
		}
	}
}
